<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info" style="font-size:14px;">
			<?php
				/**
				 * Fires before the Twenty Fifteen footer text for footer customization.
				 *
				 * @since Twenty Fifteen 1.0
				 */
				do_action( 'twentyfifteen_credits' );
			?>
			&copy; 2017 Проект студентов ОП «Политология» НИУ ВШЭ — Санкт-Петербург
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

<?php if (is_singular('map')):?>
	<script>
		var GoogleLinkURL = '<?=getGoogleLinkURL(get_the_ID())?>';
	</script>
	<script src="https://code.highcharts.com/maps/highmaps.js"></script>
	<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/mapdata/countries/ru/custom/ru-all-disputed.js"></script>
	<script type='text/javascript'>//<![CDATA[
  var $ = jQuery;

  $(document).ready(function() {

    var url = "https://spreadsheets.google.com/feeds/list/" + GoogleLinkURL + "/1/public/values?alt=json";

    var array = [];
    var values = [];

    $.getJSON(url, function(data) {

      var entry = data.feed.entry;

      $(entry).each(function(index, element){
      	if (element.gsx$data.$t.length > 0) {
      		var region = element.gsx$region.$t;
	        var code = element.gsx$code.$t;
	        var value = parseFloat(element.gsx$data.$t.replace(/ /g,"").replace(",", "."));

	        if (value.length > 0 || value != '-') {
	        	array.push({
		          'hc-key': code,
		          'title': region,
		          'value': value
		        });

		        values.push(value);

	        }
      	}
        

       
      }).ready(function() {
       $('#containerMap').highcharts('Map', {

        title: {
          text: ''
        },


        mapNavigation: {
          enabled: true,
          buttonOptions: {
            verticalAlign: 'top'
          },
          enableMouseWheelZoom: false
        },

        colorAxis: {
          min: Math.min.apply(Math, values),
          max: Math.max.apply(Math, values),
          minColor:'#ffffff',
          maxColor:'#00a58a'
        },

        navigation: {
          buttonOptions: {
            enabled: false
          }
        },


        series: [{
          data: array,
          mapData: Highcharts.maps['countries/ru/custom/ru-all-disputed'],
          joinBy: 'hc-key',
          states: {
            hover: {
              color: '#2d3745'
            }
          },
          tooltip: {
            headerFormat: '',
            pointFormat: '<span style="font-weight:bold;font-size:20px;">{point.value}</span><br/>{point.title}'
          },
          borderColor:'#000'

        }]
      });
     });


    });
  });


</script>
<?php endif;?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43688049 = new Ya.Metrika({
                    id:43688049,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43688049" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
