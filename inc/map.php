<?php

add_action('init', 'custom_post_types');
function custom_post_types() {

	global $wp_rewrite;
	
	register_post_type('map', array(
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' =>  true,
		'supports' => array('title'),
		'has_archive' => true,
		'labels' => 
		    array(
		        'name' => 'Карты',
		        'singular_name' => 'Карта',
		        'add_new' => 'Добавить карту',
		        'add_new_item' => 'Добавить новую карту',
		        'edit_item' => 'Редактировать карту',
		        'new_item' => 'Новая карта',
		        'all_items' => 'Все карты',
		        'view_item' => 'Просмотр карты',
		        'search_items' => 'Искать карту',
		        'not_found' =>  'Не найдено карт',
		        'not_found_in_trash' => 'Не найдено карт в корзине', 
		        'parent_item_colon' => '',
		        'menu_name' => 'Карты'
	        ),
		'menu_icon' => 'dashicons-admin-site',
		'taxonomies' => array('group')
	));

	register_taxonomy(
		'group',
		array('map'),
		array(
			'labels' => array(
				'name' => 'Категории',
				'singular_name' => 'Категория',
				'new_item_name' => 'Добавить категорию',
				'all_items' => 'Все категории',
				'edit_item' => 'Редактировать категорию',
				'update_item' => 'Обновить категорию',
				'add_new_item' => 'Добавить категорию',
				'new_item_name' => 'Название категории',
				'view_item' => 'Перейти на страницу категории',
		        'search_items' => 'Искать категорию',
		        'not_found' =>  'Не найдено категорий',
			),
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'sort' => true,
			'show_in_nav_menus' => false,
			'rewrite' => array('slug' => 'group', 'with_front' => false)
		)
	);

	



}


function add_custom_box() {
  $screens = array( 'map');
  foreach ( $screens as $screen )
    add_meta_box( 'googleLinkBox', 'Ссылка на Google Spreadsheets', 'googleLink_meta_box_callback', $screen );
	add_meta_box( 'sourceTextBox', 'Название источника', 'sourceText_meta_box_callback', $screen );
	add_meta_box( 'sourceLinkBox', 'Ссылка на источник', 'sourceLink_meta_box_callback', $screen );
	add_meta_box( 'souceTimeBox', 'За какое время представлены данные?', 'sourceTime_meta_box_callback', $screen );
    
}

add_action('add_meta_boxes', 'add_custom_box');


function googleLink_meta_box_callback() {
	global $post;
	?>
	<input type="url" name="googleLinkURL" id="googleLinkURL" value="<?=get_post_meta($post->ID, 'googleLinkURL', true) ?>" style="width:100%"/>
	<?php
}

function sourceLink_meta_box_callback() {
	global $post;
	?>
	<input type="url" name="sourceLink" id="sourceLink" value="<?=get_post_meta($post->ID, 'sourceLink', true) ?>" style="width:100%"/>
	<?php
}

function sourceText_meta_box_callback() {
	global $post;
	?>
	<input type="text" name="sourceText" id="sourceText" value="<?=get_post_meta($post->ID, 'sourceText', true) ?>" style="width:100%"/>
	<?php
}

function sourceTime_meta_box_callback() {
	global $post;
	?>
	<input type="text" name="sourceTime" id="sourceTime" value="<?=get_post_meta($post->ID, 'sourceTime', true) ?>" style="width:100%" placeholder="например, 2016 год"/>
	<?php
}

function save_postdata( $post_id ) {

    // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
  // Убедимся что поле установлено.

  if (isset($_REQUEST['googleLinkURL'])):
	update_post_meta($post_id, 'googleLinkURL', htmlspecialchars($_POST['googleLinkURL']) );
  endif;
  if (isset($_REQUEST['sourceLink'])):
	update_post_meta($post_id, 'sourceLink', htmlspecialchars($_POST['sourceLink']) );
  endif;
  if (isset($_REQUEST['sourceText'])):
	update_post_meta($post_id, 'sourceText', htmlspecialchars($_POST['sourceText']) );
  endif;
  if (isset($_REQUEST['sourceTime'])):
	update_post_meta($post_id, 'sourceTime', htmlspecialchars($_POST['sourceTime']) );
  endif;

}
add_action( 'save_post', 'save_postdata' );



?>