<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		twentyfifteen_post_thumbnail();
	?>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div id="containerMap">

		</div>
		
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php if (get_post_meta(get_the_ID(), 'sourceText', true)):?>
			Источник:
			<?php if (get_post_meta(get_the_ID(), 'sourceLink', true)):?>
				<a href="<?=get_post_meta(get_the_ID(), 'sourceLink', true)?>" target="_blank">
			<?php endif;?>

				<?=get_post_meta(get_the_ID(), 'sourceText', true)?>

			<?php if (get_post_meta(get_the_ID(), 'sourceLink', true)):?>
				</a>
			<?php endif;?>
			<br/>
		<?php endif;?>
		<?php if (get_post_meta(get_the_ID(), 'sourceTime', true)):?>
			Данные представлены за <?=get_post_meta(get_the_ID(), 'sourceTime', true)?>
		<?php endif;?>
	</footer>

</article><!-- #post-## -->
